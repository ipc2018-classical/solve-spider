import sys

instances =  {
    "psat-4-1-10-7-2" : range (34,42), 
    "psat-4-1-09-6-2" : range (30,38), 
    "psat-2-2-09-6-2" : range (29,38), 
    "psat-2-2-10-7-2" : range (39,52), 
    "psat-1-4-10-7-2" : range (42,58), 
    "psat-1-4-09-6-2" : range (32,45), 
    "psat-4-1-08-6-2" : range (24,35), 
    "psat-2-2-11-8-2" : range (34,51), 
    "psat-4-1-12-8-2" : range (34,53), 
    "psat-2-2-12-8-2" : range (42,68), 
    "psat-4-1-11-8-2" : range (30,50), 
    "psat-1-4-11-8-2" : range (38,66)
}

runs = [(instance, weight, bound) for instance in instances for bound in instances[instance] for weight in range(0, 8)]

if len(sys.argv) < 2:
    print(len(runs))
else:
    selected_run = runs[int(sys.argv[2])-1]
    if sys.argv[1] == "task":
        print (selected_run[0])
        
    if sys.argv[1] == "weight":
        print (selected_run[1])

    if sys.argv[1] == "bound":
        print (selected_run[2])

