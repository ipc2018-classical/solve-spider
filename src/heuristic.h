#ifndef HEURISTIC_H
#define HEURISTIC_H

class State;

class GapHeuristic {
public:
    int compute(const State &state);
};


class AdmissibleHeuristic {
public:
    int compute(const State &state);
};

#endif
