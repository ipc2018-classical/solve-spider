#ifndef STATE_H
#define STATE_H

#include <cstdint>
#include <vector>
#include <iostream>

using StateID = int;

struct Card {
    int deck;
    int suit;
    int value;
public:
    Card(int deck, int suit, int value)
        : deck(deck), suit(suit), value(value) {
    }

    void dump();

    friend std::ostream& operator<<(std::ostream& os, const Card& c) ;
};


using Pile = std::vector<Card>;
using Piles = std::vector<Pile>;

struct Substack {
    int suit : 4;
    int from_value : 6;
    int to_value : 6;

    Substack(int suit, int from_value, int to_value)
        : suit(suit), from_value(from_value), to_value(to_value) {
    }

    bool operator==(const Substack &other) const {
        return (suit == other.suit && from_value == other.from_value && to_value == other.to_value);
    }

    bool operator!=(const Substack &other) const {
        return !(*this == other);
    }
};

using SubstackPile = std::vector<Substack>;
using SubstackPiles = std::vector<SubstackPile>;

class StatePiles;

struct State {
    int current_deal;
    SubstackPiles piles;
public:
    // HACK
    State() = default;

    State(const Piles &piles);
    State(const StatePiles &piles);

    bool is_goal() const;
    void dump() const;

    int get_current_deal() const {
	return current_deal;
    }
	

    bool operator==(const State &other) const {
        return (current_deal == other.current_deal && piles == other.piles);
    }

    bool operator!=(const State &other) const {
        return !(*this == other);
    }
};

class StatePiles {
    int current_deal;
    Piles piles;
public:
    StatePiles(const Piles &piles_) : piles(piles_) {}

    bool is_goal() const;
    void dump() const;

    void collect_deck(int pile_id, std::ostream & out);
    void deal(std::ostream & out);
    void move_card_to_card(int source_pile,
			   int target_pile, int target_to_value,
			   std::ostream & out);

    void move_card_to_tableau(int source_pile, int source_value,
			      int target_pile, std::ostream & out);

    const Piles & get_piles() const {
	return piles;
    }
    int get_current_deal() const {
	return current_deal;
    }


};


#endif
