#include "globals.h"

#include "state.h"

using namespace std;

Pile read_pile(std::istream &in) {
    Pile pile;
    int n;
    in >> n;
    pile.reserve(n);
    for (int i = 0; i < n; ++i) {
        int deck, suit, value;
        in >> deck >> suit >> value;
        pile.push_back(Card(deck, suit, value));
    }
    return pile;
}

Piles read_piles(std::istream &in) {
    Piles piles;
    int n;
    in >> n;
    piles.reserve(n);
    for (int i = 0; i < n; ++i) {
        piles.push_back(read_pile(in));
    }
    return piles;
}

void read_everything(std::istream &in) {
    in >> g_num_decks >> g_num_suits >> g_num_values;
    g_deals = read_piles(in);
    Piles piles = read_piles(in);
    g_initial_state = unique_ptr<State>(new State(piles));
    g_initial_state_piles = unique_ptr<StatePiles>(new StatePiles(piles));
    g_num_piles_heuristic = g_initial_state->piles.size();
}

int g_num_decks;
int g_num_suits;
int g_num_values;
Piles g_deals;
unique_ptr<State> g_initial_state;
unique_ptr<StatePiles> g_initial_state_piles;
int g_num_piles_heuristic;
