#ifndef GLOBALS_H
#define GLOBALS_H

#include "state.h"

#include <iostream>
#include <memory>
#include <vector>

void read_everything(std::istream &in);

extern int g_num_decks;
extern int g_num_suits;
extern int g_num_values;
extern Piles g_deals;
extern std::unique_ptr<State> g_initial_state;
extern std::unique_ptr<StatePiles> g_initial_state_piles;

extern int g_num_piles_heuristic;

#endif
