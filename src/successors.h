#ifndef SUCCESSORS_H
#define SUCCESSORS_H

#include "state.h"

#include <vector>
#include <iostream>

std::vector<State> get_successors(const State &state);

void print_action_name(const State &state, const State &successor, StatePiles & state_piles, std::ostream & out);

#endif
