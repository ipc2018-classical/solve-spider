#ifndef OPEN_LIST_DEALS_H
#define OPEN_LIST_DEALS_H

#include "state.h"

#include <deque>
#include <map>

class OpenListDeals {
    mutable int max_expansions_per_list;
    mutable int num_expansions_open_list;
    mutable int current_open_list;

    std::vector<std::map<std::pair<int, int>, std::deque<StateID>>> entries_by_deal;
    void skip_empty_buckets();

    int best_f_value;
    int expanded;

    void switch_deals_queue() const;

    const std::map<std::pair<int, int>, std::deque<StateID>> & get_current_entries() const;

    std::map<std::pair<int, int>, std::deque<StateID>> & get_current_entries();

public:
    OpenListDeals(); 

    void insert(StateID s_id, const State & state, int g, int h);
    StateID pop_min();

    bool empty() const;


};

#endif
