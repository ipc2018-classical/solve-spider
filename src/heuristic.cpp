#include "heuristic.h"

#include "globals.h"
#include "state.h"

#include <cassert>
#include <vector>
#include <unordered_map>

using namespace std;

// using DependencyGraph = vector<vector<int>>;

// template <typename T1, typename T2>
// struct hash<pair<T1, T2>> {
//     std::size_t operator()(const pair<T1, T2>& p) const {
//         return hash<T1>()(p.first) ^ hash<T2>()(p.second);
//     }
// };

// DependencyGraph build_dependency_graph(const State &state) {
//     assert(g_num_decks == 1);

//     unordered_map<pair<int, int>, int> position_to_node_index;
//     unordered_map<pair<int, int>, int> card_to_node_index;
//     int num_nodes = 0;
//     for (int pile_id = 0; pile_id < state.piles.size(); ++pile_id) {
//         for (int stack_id = 0; stack_id < state.piles.size(); ++stack_id) {
//             position_to_node_index[make_pair(pile_id, stack_id)] = num_nodes;
//             Substack s = state.piles[pile_id][stack_id];
//             int suit = s.suit;
//             for (int value = s.from_value; value >= s.to_value; --value) {
//                 card_to_node_index[make_pair(suit, value)] = num_nodes;
//             }
//             ++num_nodes;
//         }
//     }

//     DependencyGraph graph(num_nodes);

//     for (int pile_id = 0; pile_id < state.piles.size(); ++pile_id) {
//         for (int stack_id = 0; stack_id < state.piles.size(); ++stack_id) {
//         }
//     }

//     return graph;
// }

// int find_min_feedback_set(const DependencyGraph &graph) {

// }


int GapHeuristic::compute(const State &state) {
    int h = 0;
    // Every stack needs to be moved or discarded once
    for (const SubstackPile &pile : state.piles) {
        h += pile.size();
    }

    return h;
}



int AdmissibleHeuristic::compute(const State &state) {
    int h = 0;
    // Every stack needs to be moved or discarded once
    for (const SubstackPile &pile : state.piles) {
        h += pile.size();
    }

    int num_remaining_deals = g_deals.size() - state.current_deal;
    // Every deal must be dealt
    h += num_remaining_deals;     
    
    //  // Every card in a deal must be moved once
    //  // NOTE: this is not admissible because a card might be dealt on a matching card
    h += num_remaining_deals * (g_num_piles_heuristic);

    // for (int i = 0; i < state.piles.size() ++i) {
    // 	for (int j = state.current_deal; j < g_deals.size(); ++j) {
    // 	    if () {
		
    // 	    }
    // 	}
    // }
    

    // //  // If there are deals left, any empty piles need to be filled once to be able to deal.
    // //  // NOTE: this will overlap with the landmarks we compute below
    //  if (num_remaining_deals > 0) {
    //      for (const SubstackPile &pile : state.piles) {
    //          if (pile.empty()) {
    //              h += 1;
    //          }
    //      }
    //  }

    //  // if (g_num_decks == 1) { 
    //  // 	DependencyGraph graph = build_dependency_graph(state);
    //  // 	h += find_min_feedback_set(graph);
    //  // }
    return h;
}

