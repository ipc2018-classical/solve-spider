#include "open_list_deals.h"

#include <cassert>
#include <iostream>
#include "globals.h"

using namespace std;

OpenListDeals::OpenListDeals () :  best_f_value(0), expanded(0), max_expansions_per_list (100), 
		       num_expansions_open_list (0),
		       current_open_list (0) {
    entries_by_deal.resize(g_deals.size() + 1);
}


void OpenListDeals::skip_empty_buckets() {
    auto & entries = get_current_entries();
    while (!entries.empty() && entries.begin()->second.empty()) {
        entries.erase(entries.begin());
    }
}

bool OpenListDeals::empty() const {
    auto & entries = get_current_entries();
    return entries.empty();
}

void OpenListDeals::insert(StateID s_id, const State & state, int g, int h) {
    entries_by_deal[state.get_current_deal()] [make_pair(h, g)].push_back(s_id);
}

const std::map<std::pair<int, int>, std::deque<StateID>> & OpenListDeals::get_current_entries() const {
    int i = 0;
    while(entries_by_deal[current_open_list].empty()) {
	switch_deals_queue();
	if (i++ > entries_by_deal.size())
	    break;
    }
    return entries_by_deal[current_open_list];
}

std::map<std::pair<int, int>, std::deque<StateID>> & OpenListDeals::get_current_entries()  {
    int i = 0;
    while(entries_by_deal[current_open_list].empty()) {
	switch_deals_queue();
	if (i++ > entries_by_deal.size())
	    break;
    }
    return entries_by_deal[current_open_list];
}


void OpenListDeals::switch_deals_queue() const {   
    num_expansions_open_list = 0;

    current_open_list++;
    if (current_open_list == entries_by_deal.size()) {
	current_open_list = 0;
	max_expansions_per_list *= 2;
    }
}
StateID OpenListDeals::pop_min() {
    if (num_expansions_open_list++ >= max_expansions_per_list) {
	switch_deals_queue();
    }
      
    skip_empty_buckets();

    auto & entries = get_current_entries();
    assert(!entries.empty());

    int f = entries.begin()->first.first;
    if (f > best_f_value) {
        best_f_value = f;
        cout << "New best f value: " << f << " (expanded: " << expanded
             << ", size of current bucket: " << entries.begin()->second.size()
             << ")" << endl;
    }
    ++expanded;

    auto &min_bucket = entries.begin()->second;
    assert(!min_bucket.empty());
    StateID s_id = min_bucket.front();
    min_bucket.pop_front();
    skip_empty_buckets();
    return s_id;
}
