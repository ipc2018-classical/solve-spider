#include "state_registry.h"

#include "globals.h"

using namespace std;

void state_to_packed_state(const State &state, PackedStateBin *data) {
    int size = 2 + state.piles.size();
    for (const SubstackPile &pile : state.piles) {
        size += pile.size();
    }
    *data = state.current_deal;
    ++data;
    *data = state.piles.size();
    ++data;
    for (const SubstackPile &pile : state.piles) {
        *data = pile.size();
        ++data;
        for (const Substack &stack : pile) {
            *data = *reinterpret_cast<const uint16_t *>(&stack);
            ++data;
        }
    }
}

State packed_state_to_state(const PackedStateBin *data) {
    State state;

    state.current_deal = *data;
    ++data;

    state.piles.resize(*data);
    ++data;

    for (SubstackPile &pile : state.piles) {
        int num_stacks = *data;
        ++data;
        pile.reserve(num_stacks);
        for (int i = 0; i < num_stacks; ++i) {
            pile.push_back(*reinterpret_cast<const Substack *>(data));
            ++data;
        }
     }

    return state;
}

int compute_state_size() {
    int num_piles = g_initial_state->piles.size();
    int num_cards = g_num_decks * g_num_suits * g_num_values;
    return 2 + num_piles + num_cards;
}

StateRegistry::StateRegistry()
    : state_size(compute_state_size()),
      state_data_pool(state_size),
      registered_states(0,
                        StateIDSemanticHash(state_data_pool, state_size),
                        StateIDSemanticEqual(state_data_pool, state_size)) {
}

State StateRegistry::lookup_state(int id) {
    PackedStateBin *data = state_data_pool[id];
    return packed_state_to_state(data);
}

int StateRegistry::get_state_id(const State &state) {
    PackedStateBin *data = new PackedStateBin[state_size];
    state_to_packed_state(state, data);
    state_data_pool.push_back(data);

    StateID id = state_data_pool.size() - 1;
    pair<StateIDSet::iterator, bool> result = registered_states.insert(id);
    bool is_new_entry = result.second;
    if (!is_new_entry) {
        state_data_pool.pop_back();
    }
    assert(registered_states.size() == state_data_pool.size());
    return *result.first;
}
