#include "state.h"

#include "globals.h"

#include <iostream>
#include <cassert>

using namespace std;


State::State(const StatePiles &piles) : State(piles.get_piles()) {
    current_deal = piles.get_current_deal();
}

State::State(const Piles &piles_) {
    piles.resize(piles_.size());
    int pile_id = 0;
    for (const Pile &pile : piles_) {
        Substack substack = {-1, -1, -1};
        for (Card card : pile) {
            if (substack.suit == card.suit && substack.to_value - 1 == card.value) {
                --substack.to_value;
            } else {
                if (substack.suit != -1) {
                    piles[pile_id].push_back(substack);
                }
                substack = Substack{card.suit, card.value, card.value};
            }
        }
        if (substack.suit != -1) {
            piles[pile_id].push_back(substack);
        }
        ++pile_id;
    }
}

void Card::dump() {
    cout << "s" << suit << "v" << value;
}

bool State::is_goal() const {
    for (const SubstackPile &pile : piles) {
        if (!pile.empty()) {
            return false;
        }
    }
    return g_deals.size() == current_deal;
}

void State::dump() const {
    cout << "Current deal: " << current_deal << endl;
    cout << endl;
    for (const SubstackPile &pile : piles) {
        for (Substack s : pile) {
            cout << "Suit " << s.suit << " from " << s.from_value << " to " << s.to_value << " | ";
        }
        cout << endl;
    }
}



void StatePiles::deal(std::ostream & out) {
    const Pile &deal = g_deals[current_deal];
	
    for (int pile_id = 0; pile_id < piles.size(); ++pile_id) {
	int deal_id = deal.size() - 1 - pile_id;
        Card c = deal[deal_id];
        piles[pile_id].push_back(deal[deal_id]);
    }
    current_deal ++;
    out << "start-dealing" << endl;
}


void StatePiles::move_card_to_card(int source_pile,
				   int target_pile, int target_to_value,
				   std::ostream & out){
    

    std::vector<Card> tmp;
    assert (!piles[source_pile].empty());
    while(tmp.empty() || tmp.back().value != target_to_value - 1) {
	assert (!piles[source_pile].empty());
	tmp.push_back(piles[source_pile].back());
	piles[source_pile].pop_back();
    }
    Card source_card = tmp.back();
    Card target_card = piles[target_pile].back();
    while(!tmp.empty()) {
	piles[target_pile].push_back(tmp.back());
	tmp.pop_back();
    }

    if (!piles[source_pile].empty()) {
	Card from_card = piles[source_pile].back();
	out << "move-to-card " << source_card <<  " " << from_card <<  " " <<target_card << " pile-" << target_pile << endl;
    }else {
	out << "move-to-card " << source_card << " pile-" << source_pile <<  " " << target_card << " pile-" << target_pile <<endl;
    }
}

void StatePiles::dump() const {
    cout << "Current deal: " << current_deal << endl;
    cout << endl;
    int pile_id = 0;
    for (const Pile &pile : piles) {
	cout << "Pile" << pile_id++ << ": "; 
	for (auto card : pile) {
            cout << "d" << card.deck <<  "-s" << card.suit << "-v" << card.value;
        }
        cout << endl;
    }
}

void StatePiles::collect_deck(int pile_id, std::ostream & out) {
    Pile & pile = piles[pile_id];
    Card last_card = pile.back();
    while(pile.size() > 1 && pile[pile.size() - 1].suit == pile[pile.size() - 2].suit
	  && pile[pile.size() - 1].value == pile[pile.size() - 2].value - 1) {
	pile.pop_back();
    }
    pile.pop_back();

    out << "start-collecting-deck " << last_card << endl;
}


void StatePiles::move_card_to_tableau(int source_pile, int source_value,
				      int target_pile, std::ostream & out){
    std::vector<Card> tmp;
    while(tmp.empty() || tmp.back().value != source_value) {
	assert (!piles[source_pile].empty());
	tmp.push_back(piles[source_pile].back());
	piles[source_pile].pop_back();
    }
    Card source_card = tmp.back();
    Card target_card = piles[target_pile].back();
    while(!tmp.empty()) {
	piles[target_pile].push_back(tmp.back());
	tmp.pop_back();
    }

    if (!piles[source_pile].empty()) {
	Card from_card = piles[source_pile].back();
	out << "move-to-tableau " << source_card <<  " " << from_card <<  " pile-" << target_pile << endl;
    }else {
	out << "move-to-tableau " << source_card << " pile-" << source_pile << " pile-" << target_pile <<endl;
    }
}



  
ostream& operator<<(ostream& os, const Card& card)  {

    os << "card-d" << card.deck << "-s" << card.suit << "-v" << card.value;
    return os;
}  
