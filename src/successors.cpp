#include "successors.h"

#include "globals.h"
#include <cassert>

using namespace std;


void create_move_card_to_card_successor(
    const State &state,
    int source_pile_id,
    int target_pile_id,
    bool full_stack,
    bool matching_suit,
    vector<State> &successors) {
    const SubstackPile &source_pile = state.piles[source_pile_id];
    const SubstackPile &target_pile = state.piles[target_pile_id];
    if (source_pile.empty() || target_pile.empty()) {
        return;
    }
    Substack source_stack = source_pile.back();
    Substack target_stack = target_pile.back();

    if (matching_suit != (target_stack.suit == source_stack.suit)) {
        return;
    }

    int next_value = target_stack.to_value - 1;
    if ((full_stack && source_stack.from_value != next_value) ||
        (!full_stack && source_stack.from_value <= next_value) ||
        (source_stack.to_value > next_value)) {
        return;
    }

    State succ_state(state);
    SubstackPile &succ_source_pile = succ_state.piles[source_pile_id];
    SubstackPile &succ_target_pile = succ_state.piles[target_pile_id];
    if (full_stack) {
        succ_source_pile.pop_back();
    } else {
        succ_source_pile.back().to_value = target_stack.to_value;
    }
    if (matching_suit) {
        succ_target_pile.back().to_value = source_stack.to_value;
    } else {
        succ_target_pile.push_back(Substack(source_stack.suit, next_value, source_stack.to_value));
    }
    successors.push_back(move(succ_state));
}

void create_move_card_to_card_successors(
    const State &state,
    bool full_stack,
    bool matching_suit,
    vector<State> &successors) {
    for (int source_pile_id = 0; source_pile_id < state.piles.size(); ++source_pile_id) {
        for (int target_pile_id = 0; target_pile_id < state.piles.size(); ++target_pile_id) {
            create_move_card_to_card_successor(
                state, source_pile_id, target_pile_id, full_stack, matching_suit, successors);
        }
    }
}

void create_discard_successor(const State &state, int pile_id, vector<State> &successors) {
    const SubstackPile &pile = state.piles[pile_id];
    if (pile.empty()) {
        return;
    }
    Substack s = pile.back();
    if (s.from_value == g_num_values - 1 && s.to_value == 0) {
        State succ_state(state);
        succ_state.piles[pile_id].pop_back();
        successors.push_back(move(succ_state));
    }
}

void create_discard_successors(const State &state, vector<State> &successors) {
    if (state.current_deal < g_deals.size()) {
        // don't discard if discarding would leave too few cards to do a deal.
        int num_cards_in_play = 0;
        for (const SubstackPile &pile : state.piles) {
            for (Substack s : pile) {
                num_cards_in_play += s.from_value - s.to_value + 1;
            }
        }
        if (num_cards_in_play - g_num_values < state.piles.size()) {
            return;
        }
    }

    for (int pile_id = 0; pile_id < state.piles.size(); ++pile_id) {
        create_discard_successor(state, pile_id, successors);
    }
}

void create_move_card_to_empty_successors(
    const State &state,
    int source_pile_id,
    int target_pile_id,
    bool full_stack,
    vector<State> &successors) {
    const SubstackPile &source_pile = state.piles[source_pile_id];
    const SubstackPile &target_pile = state.piles[target_pile_id];
    if (source_pile.empty() || !target_pile.empty()) {
        return;
    }
    if (state.current_deal == g_deals.size() && source_pile.size() == 1) {
        // Don't move cards between empty stacks if there are no deals left.
        return;
    }
    Substack source_stack = source_pile.back();

    if (full_stack) {
        State succ_state(state);
        SubstackPile &succ_source_pile = succ_state.piles[source_pile_id];
        SubstackPile &succ_target_pile = succ_state.piles[target_pile_id];
        succ_source_pile.pop_back();
        succ_target_pile.push_back(source_stack);
        successors.push_back(move(succ_state));
    } else {
        for (int value = source_stack.from_value - 1; value >= source_stack.to_value; --value) {
            State succ_state(state);
            SubstackPile &succ_source_pile = succ_state.piles[source_pile_id];
            SubstackPile &succ_target_pile = succ_state.piles[target_pile_id];
            succ_source_pile.back().to_value = value + 1;
            succ_target_pile.push_back(Substack(source_stack.suit, value, source_stack.to_value));
            successors.push_back(move(succ_state));
        }
    }
}

void create_move_card_to_empty_successors(
    const State &state,
    bool full_stack,
    vector<State> &successors) {
    for (int source_pile_id = 0; source_pile_id < state.piles.size(); ++source_pile_id) {
        for (int target_pile_id = 0; target_pile_id < state.piles.size(); ++target_pile_id) {
            create_move_card_to_empty_successors(
                state, source_pile_id, target_pile_id, full_stack, successors);
        }
    }
}

void create_deal_successor(const State &state, vector<State> &successors) {
    if (g_deals.size() == state.current_deal) {
        return;
    }
    for (const SubstackPile &pile : state.piles) {
        if (pile.empty()) {
            return;
        }
    }
    assert (state.current_deal < g_deals.size());
    const Pile &deal = g_deals[state.current_deal];
    State succ_state(state);
    succ_state.current_deal = state.current_deal + 1;
    for (int pile_id = 0; pile_id < state.piles.size(); ++pile_id) {
	int deal_id = deal.size() - 1 - pile_id;
        Card c = deal[deal_id];
        SubstackPile &succ_pile = succ_state.piles[pile_id];
        Substack &last_stack = succ_pile.back();
        if (c.suit == last_stack.suit && c.value == last_stack.to_value - 1) {
            --last_stack.to_value;
        } else {
            succ_pile.emplace_back(c.suit, c.value, c.value);
        }
    }
    successors.push_back(move(succ_state));
}

vector<State> get_successors(const State &state) {
    vector<State> successors;
    create_discard_successors(state, successors);
    create_move_card_to_card_successors(state, true, true, successors);
    create_move_card_to_card_successors(state, false, true, successors);
    create_move_card_to_card_successors(state, true, false, successors);
    create_move_card_to_card_successors(state, false, false, successors);
    create_move_card_to_empty_successors(state, true, successors);
    create_move_card_to_empty_successors(state, false, successors);
    create_deal_successor(state, successors);
    return successors;
}





void print_action_name_move_card_to_card_successor(
    const State &state,
    int source_pile_id,
    int target_pile_id,
    bool full_stack,
    bool matching_suit,
    const State &successor, StatePiles & state_piles, ostream & out) {
    const SubstackPile &source_pile = state.piles[source_pile_id];
    const SubstackPile &target_pile = state.piles[target_pile_id];
    if (source_pile.empty() || target_pile.empty()) {
        return;
    }
    Substack source_stack = source_pile.back();
    Substack target_stack = target_pile.back();

    if (matching_suit != (target_stack.suit == source_stack.suit)) {
        return;
    }

    int next_value = target_stack.to_value - 1;
    if ((full_stack && source_stack.from_value != next_value) ||
        (!full_stack && source_stack.from_value <= next_value) ||
        (source_stack.to_value > next_value)) {
        return;
    }

    State succ_state(state);
    SubstackPile &succ_source_pile = succ_state.piles[source_pile_id];
    SubstackPile &succ_target_pile = succ_state.piles[target_pile_id];
    if (full_stack) {
        succ_source_pile.pop_back();
    } else {
        succ_source_pile.back().to_value = target_stack.to_value;
    }
    if (matching_suit) {
        succ_target_pile.back().to_value = source_stack.to_value;
    } else {
        succ_target_pile.push_back(Substack(source_stack.suit, next_value, source_stack.to_value));
    }
    if (succ_state == successor) {
	
	state_piles.move_card_to_card(source_pile_id, target_pile_id, target_stack.to_value, out);

	if (succ_state != State(state_piles)) {

	    state.dump();
	    cout << endl << endl;
	    succ_state.dump();
	    cout << endl << endl;
	    State(state_piles).dump();
	}
	assert(succ_state == State(state_piles));

    }
}

void print_action_name_move_card_to_card_successors(
    const State &state,
    bool full_stack,
    bool matching_suit,
    const State &successor, StatePiles & state_piles, ostream & out) {
    for (int source_pile_id = 0; source_pile_id < state.piles.size(); ++source_pile_id) {
        for (int target_pile_id = 0; target_pile_id < state.piles.size(); ++target_pile_id) {
            print_action_name_move_card_to_card_successor(
                state, source_pile_id, target_pile_id, full_stack, matching_suit, successor, state_piles, out);
        }
    }
}

void print_action_name_discard_successor(const State &state, int pile_id, const State &successor, StatePiles & state_piles, ostream & out) {
    const SubstackPile &pile = state.piles[pile_id];
    if (pile.empty()) {
        return;
    }
    Substack s = pile.back();
    if (s.from_value == g_num_values - 1 && s.to_value == 0) {
        State succ_state(state);
        succ_state.piles[pile_id].pop_back();

	if (succ_state == successor) {
	    state_piles.collect_deck(pile_id, out);
	    assert(succ_state == State(state_piles));
	}

    }
}

void print_action_name_discard_successors(const State &state, const State &successor, StatePiles & state_piles, ostream & out) {
    if (state.current_deal < g_deals.size()) {
        // don't discard if discarding would leave too few cards to do a deal.
        int num_cards_in_play = 0;
        for (const SubstackPile &pile : state.piles) {
            for (Substack s : pile) {
                num_cards_in_play += s.from_value - s.to_value + 1;
            }
        }
        if (num_cards_in_play - g_num_values < state.piles.size()) {
            return;
        }
    }

    for (int pile_id = 0; pile_id < state.piles.size(); ++pile_id) {
        print_action_name_discard_successor(state, pile_id, successor, state_piles, out);
    }
}

void print_action_name_move_card_to_empty_successors(
    const State &state,
    int source_pile_id,
    int target_pile_id,
    bool full_stack,
    const State &successor, StatePiles & state_piles, ostream & out) {
    const SubstackPile &source_pile = state.piles[source_pile_id];
    const SubstackPile &target_pile = state.piles[target_pile_id];
    if (source_pile.empty() || !target_pile.empty()) {
        return;
    }
    if (state.current_deal == g_deals.size() && source_pile.size() == 1) {
        // Don't move cards between empty stacks if there are no deals left.
        return;
    }
    Substack source_stack = source_pile.back();

    if (full_stack) {
        State succ_state(state);
        SubstackPile &succ_source_pile = succ_state.piles[source_pile_id];
        SubstackPile &succ_target_pile = succ_state.piles[target_pile_id];
        succ_source_pile.pop_back();
        succ_target_pile.push_back(source_stack);
	if (succ_state == successor) {
	    state_piles.move_card_to_tableau(source_pile_id, source_stack.from_value, target_pile_id, out);


	    assert(succ_state == State(state_piles));
	}
    } else {
        for (int value = source_stack.from_value - 1; value >= source_stack.to_value; --value) {
            State succ_state(state);
            SubstackPile &succ_source_pile = succ_state.piles[source_pile_id];
            SubstackPile &succ_target_pile = succ_state.piles[target_pile_id];
            succ_source_pile.back().to_value = value + 1;
            succ_target_pile.push_back(Substack(source_stack.suit, value, source_stack.to_value));

	    if (succ_state == successor) {
		state_piles.move_card_to_tableau(source_pile_id, value, target_pile_id, out);
		assert(succ_state == State(state_piles));
	    }
	}
    }
}

void print_action_name_move_card_to_empty_successors(
    const State &state,
    bool full_stack,
    const State &successor, StatePiles & state_piles, ostream & out) {
    for (int source_pile_id = 0; source_pile_id < state.piles.size(); ++source_pile_id) {
        for (int target_pile_id = 0; target_pile_id < state.piles.size(); ++target_pile_id) {
            print_action_name_move_card_to_empty_successors(
                state, source_pile_id, target_pile_id, full_stack, successor, state_piles, out);
        }
    }
}

void print_action_name_deal_successor(const State &state, const State &successor, StatePiles & state_piles, ostream & out) {
    if (g_deals.size() == state.current_deal) {
        return;
    }
    for (const SubstackPile &pile : state.piles) {
        if (pile.empty()) {
            return;
        }
    }
    assert (state.current_deal < g_deals.size());
    const Pile &deal = g_deals[state.current_deal];
    State succ_state(state);
    succ_state.current_deal = state.current_deal + 1;
    for (int pile_id = 0; pile_id < state.piles.size(); ++pile_id) {
	int deal_id = deal.size() - 1 - pile_id;
        Card c = deal[deal_id];
        SubstackPile &succ_pile = succ_state.piles[pile_id];
        Substack &last_stack = succ_pile.back();
        if (c.suit == last_stack.suit && c.value == last_stack.to_value - 1) {
            --last_stack.to_value;
        } else {
            succ_pile.emplace_back(c.suit, c.value, c.value);
        }
    }

    if (succ_state == successor) {
	state_piles.deal(out);
	// succ_state.dump();
	// cout << endl << endl;
	// State(state_piles).dump();
	assert(succ_state == State(state_piles));
    }
}

void print_action_name(const State &state, const State &successor, StatePiles & state_piles, ostream & out) {
    vector<State> successors;
    print_action_name_discard_successors(state, successor, state_piles, out);
    print_action_name_move_card_to_card_successors(state, true, true, successor, state_piles, out);
    print_action_name_move_card_to_card_successors(state, false, true, successor, state_piles, out);
    print_action_name_move_card_to_card_successors(state, true, false, successor, state_piles, out);
    print_action_name_move_card_to_card_successors(state, false, false, successor, state_piles, out);
    print_action_name_move_card_to_empty_successors(state, true, successor, state_piles, out);
    print_action_name_move_card_to_empty_successors(state, false, successor, state_piles, out);
    print_action_name_deal_successor(state, successor, state_piles, out);
}

