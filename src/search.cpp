#include "search.h"

#include "globals.h"
#include "successors.h"
#include "open_list.h"
#include "open_list_deals.h"

#include <algorithm>
#include <cassert>

using namespace std;

template <typename T, typename H> 
SearchEngine<T, H>::SearchEngine () {
}
template <typename T, typename H> 
Plan SearchEngine<T, H>::search(int bound, int weight) {
    int initial_state_id = registry.get_state_id(*g_initial_state);
    open.insert(initial_state_id, *g_initial_state, 0, heuristic.compute(*g_initial_state));
    SearchNode &node = search_space[initial_state_id];
    node.parent_state_id = initial_state_id;
    node.g = 0;
    node.status = SearchNodeStatus::OPEN;

    while (!open.empty()) {
        StateID s_id = open.pop_min();
        SearchNode &node = search_space[s_id];

        if (node.status == SearchNodeStatus::CLOSED) {
            continue;
        }
        node.status = SearchNodeStatus::CLOSED;
        State s = registry.lookup_state(s_id);

        if (s.is_goal()) {
            return extract_solution(s_id);
        }
	static int best_heuristic = -1;
        for (const State &succ : get_successors(s)) {
	    
            int succ_id = registry.get_state_id(succ);
            SearchNode &succ_node = search_space[succ_id];
            if (succ_node.status == SearchNodeStatus::NEW) {
		int h = heuristic.compute(succ);
		succ_node.status = SearchNodeStatus::OPEN;
                succ_node.g = node.g + 1;
                succ_node.parent_state_id = s_id;

		if (node.g + 1 + h > bound) {
		    succ_node.status = SearchNodeStatus::CLOSED;
		} else {
		    open.insert(succ_id, succ, succ_node.g, h*weight);    
		}

                if (best_heuristic == -1 || h < best_heuristic) {
                    cout << "new best heuristic value: " << h
                         << " with g value " << succ_node.g << endl;
                    // succ.dump();
                    best_heuristic = h;
                }

                                
            } else if (succ_node.g > node.g + 1) {
                succ_node.status = SearchNodeStatus::OPEN;
                succ_node.g = node.g + 1;
                succ_node.parent_state_id = s_id;

                
                int h = heuristic.compute(succ);
                if (best_heuristic == -1 || h < best_heuristic) {
                    cout << "new best heuristic value: " << h
                         << " with g value " << succ_node.g << endl;
                    // succ.dump();
                    best_heuristic = h;
                }

		if (node.g + 1 + h > bound) {
		    succ_node.status = SearchNodeStatus::CLOSED;
		} else {
		    open.insert(succ_id, succ, succ_node.g, h*weight);    
		}
            }
        }
    }
    return Plan();
}

template <typename T, typename H> 
Plan SearchEngine<T, H>::extract_solution(int s_id) {
    //cout << registry.size() << endl;
    Plan plan;
    while (s_id != search_space[s_id].parent_state_id) {
        plan.push_back(registry.lookup_state(s_id));
        s_id = search_space[s_id].parent_state_id;
    }
    plan.push_back(registry.lookup_state(s_id));
    reverse(plan.begin(), plan.end());
    return plan;
}
void x() {
    SearchEngine<OpenListDeals, GapHeuristic> s;
    SearchEngine<OpenListDeals, AdmissibleHeuristic> s3;

    SearchEngine<OpenList, AdmissibleHeuristic> s2;
    s.search();
    s2.search();
    s3.search();
    
}
