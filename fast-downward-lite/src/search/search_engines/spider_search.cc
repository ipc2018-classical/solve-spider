#include "spider_search.h"

#include "../globals.h"
#include "../heuristic.h"
#include "../option_parser.h"
#include "../plugin.h"

#include "../algorithms/ordered_set.h"
#include "../evaluators/g_evaluator.h"
#include "../evaluators/pref_evaluator.h"
#include "../open_lists/standard_scalar_open_list.h"
#include "../open_lists/tiebreaking_open_list.h"
#include "../task_utils/successor_generator.h"
#include "../utils/system.h"
#include <fstream>

using namespace std;
using utils::ExitCode;

namespace spider_search {

    SpiderSearch::SpiderSearch(const Options &opts)
	: SearchEngine(opts),
	  current_state(state_registry.get_initial_state()),
	  step_counter (0){
	search_space.get_node (current_state);

    }
    void SpiderSearch::initialize() {
        string line;
	ifstream myfile ("spider_plan");
	if (myfile.is_open()) {
	    while ( getline (myfile,line) ) {
		domain_dependent_plan.push_back(line);
	    }
	    myfile.close();
	} else {
	    cout << "Unable to open file";
	}

    }

    static inline std::string rtrim(std::string s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		    return !std::isspace(ch);
		}).base(), s.end());
	return s;
    }

    
    OperatorID SpiderSearch::find_match(const string & action,
					const vector<OperatorID> & successor_operators) const {

	for (OperatorID op_id : successor_operators) {
	    if(rtrim(task_proxy.get_operators()[op_id].get_name()) == rtrim(action)) {
		return op_id;
	    } 
	}

	return OperatorID::no_operator;
    }

SearchStatus SpiderSearch::step() {
    if (check_goal_and_set_plan(current_state)) {
        return SOLVED;
    }
    auto current_node = search_space.get_node(current_state);
    vector<OperatorID> successor_operators;
    g_successor_generator->generate_applicable_ops(current_state, successor_operators);
    // cout << successor_operators.size() << endl;
    // if (successor_operators.empty()) {
    // 	cout << " 
    // }
    
    if(successor_operators.size() > 1 ||
       task_proxy.get_operators()[successor_operators[0]].get_cost() > 0) {
	auto action = domain_dependent_plan[step_counter++];

	auto op_id = find_match(action, successor_operators);
	if (op_id != OperatorID::no_operator) {
	    
	    cout << "Selected: " << action << ": " << task_proxy.get_operators()[op_id].get_name() << endl;
	    OperatorProxy op = task_proxy.get_operators()[op_id];
	    current_state = state_registry.get_successor_state(current_state, op);
	    auto node = search_space.get_node (current_state);
	    node.open(current_node, op);
		
	    return IN_PROGRESS;
	}
	
	cout << "No match for " << action << " - FAILED" << endl;
	for (OperatorID op_id : successor_operators) {
	    auto op_name = task_proxy.get_operators()[op_id].get_name();
	    cout << op_name << endl;
	}
    } else  {
	auto op = task_proxy.get_operators()[successor_operators[0]];
	cout << "Auxiliar: " << op.get_name() << endl;
	auto new_state = state_registry.get_successor_state(current_state, op);
	current_state = new_state;

	auto node = search_space.get_node (current_state);
	node.open(current_node, op);
	return IN_PROGRESS;

    }
    
    return FAILED;
}

static shared_ptr<SearchEngine> _parse(OptionParser &parser) {
    parser.document_synopsis("Lazy enforced hill-climbing", "");

    SearchEngine::add_options_to_parser(parser);
    Options opts = parser.parse();

    if (parser.dry_run())
        return nullptr;
    else
        return make_shared<SpiderSearch>(opts);
}

static PluginShared<SearchEngine> _plugin("spider", _parse);
}
