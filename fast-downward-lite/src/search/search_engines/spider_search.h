#ifndef SEARCH_ENGINES_ENFORCED_HILL_CLIMBING_SEARCH_H
#define SEARCH_ENGINES_ENFORCED_HILL_CLIMBING_SEARCH_H

#include "../evaluation_context.h"
#include "../open_list.h"
#include "../search_engine.h"

#include <map>
#include <memory>
#include <set>
#include <utility>
#include <vector>

namespace options {
class Options;
}

namespace spider_search {

class SpiderSearch : public SearchEngine {
    std::vector<std::string> domain_dependent_plan;
    
    GlobalState current_state;

    int step_counter;


    OperatorID find_match(const std::string & action,
			  const std::vector<OperatorID> & successor_operators) const;


protected:
    virtual void initialize() override;
    virtual SearchStatus step() override;

public:
    explicit SpiderSearch(const options::Options &opts);
    virtual ~SpiderSearch() override = default;

};
}

#endif
